package com.atlassian.maven.plugins.amps.util;

import java.util.Arrays;
import java.util.List;

import com.atlassian.maven.plugins.amps.product.ProductHandlerFactory;

/**
 * @since version
 */
public interface AmpsEmailSubscriber
{
    void promptForSubscription();
}
